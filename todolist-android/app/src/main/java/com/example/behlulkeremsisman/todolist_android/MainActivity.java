package com.example.behlulkeremsisman.todolist_android;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.behlulkeremsisman.todolist_android.db.TaskContract;
import com.example.behlulkeremsisman.todolist_android.db.TaskDbHelper;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";   //TAG constant with the name of the class for logging

    private TaskDbHelper mHelper;

    private ListView mTaskListView;
    private ListView mTaskListView2;
    private ArrayAdapter mAdapter;
    private ArrayAdapter mAdapter2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("INFO","Your app is created.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button getData = (Button) findViewById(R.id.getservicedata);
        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("INFO","You are connecting to rest URL now.");
                String restURL = "http://www.androidexample.com/media/webservice/JsonReturn.php";
                new RestOperation().execute(restURL);
            }
        });

        mHelper = new TaskDbHelper(this);
        mTaskListView = (ListView) findViewById(R.id.list_todo);
        mTaskListView2 = (ListView) findViewById(R.id.list_todo);
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{TaskContract.TaskEntry._ID, TaskContract.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            Log.d(TAG, "Task: " + cursor.getString(idx));
        }
        db.close();
        updateUI();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i("INFO","You are adding task now.");

        setContentView(R.layout.todo);

        Button saveButton = (Button) findViewById(R.id.saveButton);

        Button cancelButton = (Button) findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updateUI();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        saveButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v)
            {
                final EditText todoTitle = (EditText) findViewById(R.id.todoTitle);

                final EditText todoInfo = (EditText) findViewById(R.id.todoInfo);

                SQLiteDatabase db = mHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(TaskContract.TaskEntry.COL_TASK_TITLE, String.valueOf(todoTitle.getText()));
                values.put(TaskContract.TaskEntry.COL_TASK_INFO, String.valueOf(todoInfo.getText()));
                values.put(TaskContract.TaskEntry.COL_TASK_DATE, "" + day + "/" + month + "/");
                values.put(TaskContract.TaskEntry.COL_TASK_TIME, "" + hour + ":" + minute);
                db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                db.close();

                updateUI();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        return super.onOptionsItemSelected(item);
    }




    public void editMode(View view){

        Log.i("INFO","You are editing task now.");





        setContentView(R.layout.todo);

        Button saveButton = (Button) findViewById(R.id.saveButton);

        final Button cancelButton = (Button) findViewById(R.id.cancelButton);

        final EditText todoTitle = (EditText) findViewById(R.id.todoTitle);

        final EditText todoInfo = (EditText) findViewById(R.id.todoInfo);

        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{TaskContract.TaskEntry._ID, TaskContract.TaskEntry.COL_TASK_TITLE, TaskContract.TaskEntry.COL_TASK_INFO, TaskContract.TaskEntry.COL_TASK_DATE, TaskContract.TaskEntry.COL_TASK_TIME},
                null, null, null, null, null);

        while (cursor.moveToNext()) {


            int idxTitle = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            int idxInfo = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_INFO);
            int idxDate = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_DATE);
            int idxTime = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TIME);

            todoTitle.setText(cursor.getString(idxTitle));
            todoInfo.setText(cursor.getString(idxInfo));
            //todoInfo.setText(cursor.getString(idxDate));
            //todoInfo.setText(cursor.getString(idxTime));
            if(todoInfo.getText().equals(cursor.getString(idxTitle)))
                break;
        }



        cursor.close();
        db.close();

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                updateUI();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v)
            {
                final EditText todoTitle = (EditText) findViewById(R.id.todoTitle);

                final EditText todoInfo = (EditText) findViewById(R.id.todoInfo);



                SQLiteDatabase db = mHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(TaskContract.TaskEntry.COL_TASK_TITLE, String.valueOf(todoTitle.getText()));
                values.put(TaskContract.TaskEntry.COL_TASK_INFO, String.valueOf(todoInfo.getText()));
                values.put(TaskContract.TaskEntry.COL_TASK_DATE, "" + day + "/" + month + "/");
                values.put(TaskContract.TaskEntry.COL_TASK_TIME, "" + hour + ":" + minute);
                db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                db.close();

                updateUI();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

    }

    public static int day = 0, month = 0, year = 0;
    public static int hour = 0, minute = 0;

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();

        ArrayList<String> taskInfo = new ArrayList<>();

        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{TaskContract.TaskEntry._ID, TaskContract.TaskEntry.COL_TASK_TITLE, TaskContract.TaskEntry.COL_TASK_INFO, TaskContract.TaskEntry.COL_TASK_DATE, TaskContract.TaskEntry.COL_TASK_TIME},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            int idxTitle = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            int idxInfo = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_INFO);
            taskList.add(cursor.getString(idxTitle));
            taskInfo.add(cursor.getString(idxInfo));


        }

        if (mAdapter == null) {
            mAdapter = new ArrayAdapter<>(this,
                    R.layout.item_todo, // what view to use for the items
                    R.id.task_title, // where to put the String of data
                    taskList); // where to get all the data

            mTaskListView.setAdapter(mAdapter); // set it as the adapter of the ListView instance

        } else {
            mAdapter.clear();
            mAdapter.addAll(taskList);
            mAdapter.notifyDataSetChanged();
        }


        /*if (mAdapter2 == null) {
            mAdapter2 = new ArrayAdapter<>(this,
                    R.layout.item_todo, // what view to use for the items
                    R.id.task_info, // where to put the String of data
                    taskInfo); // where to get all the data

            mTaskListView2.setAdapter(mAdapter2); // set it as the adapter of the ListView instance

        } else {
            mAdapter2.clear();
            mAdapter2.addAll(taskList);
            mAdapter2.notifyDataSetChanged();
        }*/


        cursor.close();
        db.close();
    }

    public void deleteTask(View view) {
        Log.w(TAG,"You are deleting your task!");
        View parent = (View) view.getParent();
        TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        String task = String.valueOf(taskTextView.getText());
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.delete(TaskContract.TaskEntry.TABLE,
                TaskContract.TaskEntry.COL_TASK_TITLE + " = ?",
                new String[]{task});
        db.close();
        updateUI();
    }


    private class RestOperation extends AsyncTask<String, Void, Void> {

        final HttpClient httpClient = new DefaultHttpClient();
        String content;
        String error;
        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        String data;
        TextView serverDataReceived = (TextView) findViewById(R.id.serverDataReceived);
        TextView showParsedJSON = (TextView) findViewById(R.id.showParsedJSON);
        EditText userinput = (EditText) findViewById(R.id.userinput);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setTitle("Please wait...");
            progressDialog.show();

            try {
                data += "&" + URLEncoder.encode("data", "UTF-8") + "-" + userinput.getText();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        protected Void doInBackground(String... params) {
            BufferedReader br = null;

            URL url;

            try {
                url = new URL(params[0]);
                URLConnection connection = url.openConnection();
                connection.setDoOutput(true);

                OutputStreamWriter outputStreamWr = new OutputStreamWriter(connection.getOutputStream());
                outputStreamWr.write(data);
                outputStreamWr.flush();

                br = new BufferedReader((new InputStreamReader(connection.getInputStream())));
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append(System.getProperty("line.seperator"));
                }

                content = sb.toString();

            } catch (MalformedURLException e) {
                error = e.getMessage();
                e.printStackTrace();
            } catch (IOException e) {
                error = e.getMessage();
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            progressDialog.dismiss();

            if (error != null) {
                serverDataReceived.setText("Error" + error);
            } else {
                serverDataReceived.setText(content);

                String output = "";
                JSONObject jsonResponse;

                try {
                    jsonResponse = new JSONObject(content);
                    JSONArray jsonArray = jsonResponse.optJSONArray("Android");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject child = jsonArray.getJSONObject(i);

                        String name = child.getString("name");
                        String number = child.getString("number");
                        String time = child.getString("date_added");

                        output = "Name = " + name + System.getProperty("line.seperator") + number + System.getProperty("line.seperator") + time;
                        output += System.getProperty("line.seperator");
                    }

                    showParsedJSON.setText(output);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}